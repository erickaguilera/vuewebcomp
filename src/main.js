import Vue from 'vue'
import App from './App.vue'
import DesignSystem from 'components-design-system'
import 'components-design-system/dist/system/system.css'
import vueCustomElement from 'vue-custom-element'
import 'document-register-element';

Vue.use(DesignSystem);
Vue.use(vueCustomElement);
Vue.config.productionTip = false;
Vue.customElement('cdsvue-widget', App);
